#import "/lib.typ": *

#huge-title[#heading(level: 1, outlined: false, bookmarked: true)[Backmatter]]

#large-title[#heading(level: 2, outlined: false, bookmarked: false)[Colophon]]

*Typefaces Used:* Alegreya Sans, Amarante, Asul, & Gentium Book Plus. These are
all open-source and freely available fonts, licensed under the SIL font license.

*Software Used:* This book was built with free and open-source software.
#ts-link("https://github.com/typst/typst")[Typst] was used as the typesetting
system.
#ts-link("https://github.com/VSCodium/vscodium")[VSCodium] was used as the
text-editor. All of this was done on #ts-link("https://archlinux.org/")[Arch Linux].
The only aspects of #emph(title) that is not free and open-source software is
the git repo hosting (GitHub), automated CI/CD (GitHub Actions), and the
publishing platform (Itch.io).

#large-title[#heading(level: 2, outlined: false, bookmarked: false)[Legal]]
