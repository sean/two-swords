#import "/lib.typ": *

#huge-title[= The Continent of Gresha <continent-of-gresha>]

#large-title[== Your Truths of Gresha]

There is no single correct iteration of Gresha. While the above characteristics
are assumed to be common among all Greshas, your Gresha will be different than
mine, is different to his, hers, etc. The Gresha presented here is actually in
the form of truths---your truths are what make your Gresha unique amongst all
others.

#item-title[#heading(level: 2, outlined: false)[The Rot Blight]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
  *The rot blight never ended.*
], [3–4], [
  *The rot blight ended long ago.*
], [5–6], [
  *The rot blight ended just under a year ago.*
])

#item-title[#heading(level: 2, outlined: false)[The Aeldar]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
  *The Aeldar disappeared long ago without a trace.*
], [3–4], [
  *The Aeldar were wiped out in a genocidal war.*
], [5–6], [
  *The Aeldar live among us to this day.*
])

#item-title[#heading(level: 2, outlined: false)[The Allied Kingdoms]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#item-title[#heading(level: 2, outlined: false)[The Empire]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#item-title[
  #heading(level: 2, outlined: false)[
    War between the Empire and the Northern Alliance
  ]
]

#note[
  *Note:* War can be a very touchy subject---for many, war is an unfortunate
  reality of everyday life. People often to use these games as an escape from such
  realities. If the group wants to pick a specific choice in this list, then do
  not hesitate to pick it. Additionally, like all of these truths, feel free to
  come up with your own if none of these options satisfy your group.
]

#grid(
  columns: (2em, 1fr),
  gutter: 1em,
  [1–2],
  [
    *The great war is at its height.* May the gods have mercy on us all.
  ],
  [3–4],
  [
    *The greatest war Gresha has ever faced just ended a year ago.*
  ],
  [5–6],
  [
    *Gresha is on the brink of total war between these two forces.* May the gods
    save us from this pending doom.
  ],
)

#item-title[#heading(level: 2, outlined: false)[Medicine]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#item-title[#heading(level: 2, outlined: false)[Magic]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#item-title[#heading(level: 2, outlined: false)[Religion]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#item-title[#heading(level: 2, outlined: false)[The Old Wood]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#item-title[#heading(level: 2, outlined: false)[The Withered March]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#item-title[#heading(level: 2, outlined: false)[Dragon's Reach]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#item-title[#heading(level: 2, outlined: false)[The Fertile Coast]]

#grid(columns: (2em, 1fr), gutter: 1em, [1–2], [
], [3–4], [
], [5–6], [
])

#large-title[== Regions of Gresha]

#item-title[=== The Withered March]

#item-title[=== The Old Wood]

#item-title[=== Dragon's Reach]

#item-title[=== The Fertile Coast]

#large-title[== Factions of Gresha]

#item-title[=== Political Factions]

==== The Empire

==== The Northern Alliance

==== The Rebels

#item-title[=== Religious Factions]

#item-title[=== Guilds]

==== The Syndicate of Financiers & Holdings

==== The Rangers

==== The Seekers

#large-title[== Locations to Place in Gresha]

#item-title[=== The Gloomfen]

#item-title[=== Ursheym Wood] <ursheym-wood>

Patrolling the wood are *Ursheym Golems*---built by one of the
#ts-link(<giants-of-ursheym-wood>)[giants that live here] that wishes to protect
her home.

#cairn-stat-block(
  load-stat("golem"),
)[
  #heading(level: 3)[Ursheym Golem] <foe.ursheym-golem>

  Ursheym Golems are much like regular golems. They are all made of wood, stone,
  moss, and vines. They are given the following commands:
  - Attack all of those who wish to do harm to the wood or its inhabitents.
  - Do not attack any inhabitents or friends of the Ursheym Wood.

  Each one also adorns a unique flower in its 'hair.'
]

#large-title[== Characters to Place in Gresha]

#item-title[=== The Giants of Ursheym Wood] <giants-of-ursheym-wood>

#note[
  *Note:* Unlike any of the other characters, I consider these to belong in a
  specific location: The Ursheym Wood. These characters are an homage to 'real'
  forest giants where I grew up. They are placed in Bernheim Forest (the namesake
  of Ursheym Wood)---a place I have many fond memories of.

  Read about the Giants of Bernheim Forest here:
  #ts-link("https://bernheim.org/land-of-the-giants/")
]

- *The Giants of Ursheym Wood* are a small family that live in the small forest.
- *Lamori* is giant that resides in the #ts-link(<ursheym-wood>)[Ursheym Wood].
  She lives there with her two children, Lana and Noso, and is pregnant with one
  more on the way. She likes to relax and lounge around. She does not tolerate
  those who wish to bring harm to the wood or her family, but she welcomes
  travellers who display peaceful intentions.
- *Lana* is a builder who constructs magnificent structures and other constructs
  all around the Ursheym Wood. She believes that one day she will build a great
  wonder that will bring all the people to visit the Ursheym Wood. She always
  enjoys when her little brother tells her about finding on of her constructs in
  the wood. She built all of the Ursheym Golems that patrol the wood.
- *Noso* wanders the wood looking for new plants, creatures, and 'discovering' his
  sister's constructs. He doesn't know she makes them and invents his own tale of
  each one and how they came to being. He enjoys looking at his reflection in the
  ponds scattered about the wood, and tries to reflectivity works. He also helps
  his mother prepare food.

While this family of giants do belong in Ursheym to me, still feel free to place
them wherever you wish. Maybe put an #ts-link(<foe.ursheym-golem>)[Ursheym Golem] in
a random encounter table for a wooded area, and if encountered, place this
family of giants nearby.

#item-title[=== The Witch Sisters]
