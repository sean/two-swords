#import "/lib.typ": *

#huge-title[= The Warden's Tome]

#text(weight: "bold")[#text-scale(120%)[Players read no further!]] This is the
Warden's section of #emph(title) and is meant for their eyes only! This section
contains the following:

- New procedures and supplemental rules to add mechanics that bring about dark
  settings.
- Dozens of new monsters to fill your worlds with.
- Gresha, a continent full of danger, wild magic, and personality.
- Tables and tools for generating sites, NPCs, locations, and more.

#large-title[== Procedures <procedures>]

#item-title[=== Hex Travel]

#item-title[=== Curses]

#large-title[== Tables]

#item-title[=== NPCs]

#item-title[=== Settlements]

#item-title[=== Sites]
