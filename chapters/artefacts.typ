#import "/lib.typ": *

#huge-title[= Relics & Artefacts]

#item-title[== Wriggling Fingers of Arul]

- 1 charge per finger.
- *Recharge:* Stick the last of the batch of fingers in blood-soaked soil and let
  it grow---it will create another batch of several fingers within a week. When
  one finger of the batch has been planted and sprouts new fingers, the others
  instantly turn to rotten mush.// Nice try you munchkins!
