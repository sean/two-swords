#import "/lib.typ": *

#huge-title[= Backgrounds <backgrounds>]

Here are 20 more backgrounds for new characters.

#table-title[Backgrounds]
#v(0pt, weak: true)
#grid(columns: 2, gutter: 2pt, table(
  columns: (auto, 1fr),
  [*d20*],
  [*Background*],
  [*1*],
  [#ts-link(<background.alchemist>)[Alchemist]],
  [*2*],
  [#ts-link(<background.druid>)[Druid]],
  [*3*],
  [#ts-link(<background.gravetender>)[Gravetender]],
  [*4*],
  [#ts-link(<background.herbalist>)[Herbalist]],
  [*5*],
  [#ts-link(<background.hexer>)[Hexer]],
  [*6*],
  [#ts-link(<background.mage>)[Mage]],
  [*7*],
  [#ts-link(<background.man-at-arms>)[Man at Arms]],
  [*8*],
  [#ts-link(<background.merchant>)[Merchant]],
  [*9*],
  [#ts-link(<background.minstrel>)[Minstrel]],
  [*10*],
  [#ts-link(<background.occultist>)[Occultist]],
), table(
  columns: (auto, 1fr),
  [*d20*],
  [*Background*],
  [*11*],
  [#ts-link(<background.pellar>)[Pellar]],
  [*12*],
  [#ts-link(<background.ranger>)[Ranger]],
  [*13*],
  [#ts-link(<background.seeker>)[Seeker]],
  [*14*],
  [#ts-link(<background.smith>)[Smith]],
  [*15*],
  [#ts-link(<background.street-ruffian>)[Street Ruffian]],
  [*16*],
  [#ts-link(<background.thief>)[Thief]],
  [*17*],
  [#ts-link(<background.torchbearer>)[Torchbearer]],
  [*18*],
  [#ts-link(<background.vault-keeper>)[Vault Keeper]],
  [*19*],
  [#ts-link(<background.village-hunter>)[Village Hunter]],
  [*20*],
  [#ts-link(<background.zealot>)[Zealot]],
))

#v(1em)

#block(breakable: false)[
  #large-title[== Alchemist <background.alchemist>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Druid <background.druid>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(
  breakable: false,
)[
  #large-title[== Gravetender <background.gravetender>]

  Death is only the beginning for some. It's your job to make sure those that seek
  fresh corpses, like ghouls and grave hags, stay away from those that lay in
  their final resting places.

  #item-title[=== Starting Gear]
  - Lantern
  - Lantern oil (6 uses)
  - Arming Sword (d8)
  - Chains (10ft)

  #item-title[=== Names]

  *Masculine Names:* Thaller

  *Feminine Names:*

  *Surnames:*

  #table-title[What did you take from the dead?]
  #v(0pt, weak: true)
  #table(
    columns: (auto, 1fr),
    [*d6*],
    [*Result*],
    [*1*],
    [A *Crow-Shaped Amulet*. You can ask a question of the dead, but must add a _Fatigue_
      each time.],
    [*2*],
    [A Crow-Shaped Amulet. You can ask a question of the dead, but must add a Fatigue
      each time.],
    [*3*],
    [A Crow-Shaped Amulet. You can ask a question of the dead, but must add a Fatigue
      each time.],
    [*4*],
    [A Crow-Shaped Amulet. You can ask a question of the dead, but must add a Fatigue
      each time.],
    [*5*],
    [A Crow-Shaped Amulet. You can ask a question of the dead, but must add a Fatigue
      each time.],
    [*6*],
    [A Crow-Shaped Amulet. You can ask a question of the dead, but must add a Fatigue
      each time.],
  )
]

#v(1em)

#block(breakable: false)[
  #large-title[== Herbalist <background.herbalist>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Hexer <background.hexer>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Mage <background.mage>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Man at Arms <background.man-at-arms>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Merchant <background.merchant>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Minstrel <background.minstrel>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Occultist <background.occultist>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Pellar <background.pellar>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Ranger <background.ranger>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Seeker <background.seeker>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Smith <background.smith>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Street Ruffian <background.street-ruffian>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Thief <background.thief>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Torchbearer <background.torchbearer>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Vault Keeper <background.vault-keeper>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Village Hunter <background.village-hunter>]

  #item-title[=== Starting Gear]
]

#v(1em)

#block(breakable: false)[
  #large-title[== Zealot <background.zealot>]

  #item-title[=== Starting Gear]
]
