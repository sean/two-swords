#import "/lib.typ": *

#huge-title[= The Setting of Gresha]

Gresha (IPA: [griʃa]) is the assumed setting of #emph(title). It is a huge
continental land-mass that sits in what is called the southern hemisphere on
Earth. The year begins and ends in the hot months, with the cold months in the
middle. The dry and hot regions like are in the North of Gresha, and the cold
and frigid regions are to the South.

#large-title[#heading(level: 2, outlined: false)[Some Assumptions]]

#note[
  *Note:* I can't stop you from tweaking these, but just be aware that if you do,
  it will diminish the tone set out by this book.
]

While everyone's Gresha will be quite unique, there are still aspects that are
shared between them all.

*Gresha is dangerous.* The lands that reside within Gresha are filled with
monsters that lurk and prowl. People, too, fill these lands and they wage war,
lie, steal, take, and kill. Innocence is hunted and the weak are tested.

*Gresha is not 'gonzo' fantasy.* Mile-tall volcanoes that sit upon huge floating
islands don't exist in Gresha. No character on their own is a threat to the
world. Seeing owlbears shooting fireballs out of a tentacled ship probably means
you ate some neshrooms.

*Gresha is fantastical.* Ancient civilizations have left undiscovered ruins,
mythical creatures and hideous monsters roam the lands, wizards sit in their
towers experimenting, and artefacts of legend rest in dark corners. Every
settlement to rest in has a story and every character to meet has a past.

*Gresha is for the apathetic.* People here are not looking to be heroes. They do
not want to leave the comfort of the walls that keep the monsters out of their
small towns. They do not ask "what is over the horizon?" They certainly will not
risk their lives to save others---at least, not for free.

*Gresha is not hopeless.* Yes, evil is everywhere; Yes, war is hell; Yes, trying
to be a hero is suicide. There is also comfort in simplicity and the mundane.
People seek a reason to dance, to enjoy the day, to celebrate a life, and to
enjoy the comfort of those they love. While these glimpses of a normal life are
not an every day occurence, they happen and stand out amongst the grey.

#large-title[== Calendar of Gresha]

- Days are 24 hours long.
- Weeks are 9 days long.
- Months are 5 weeks long.
- There are 8 months in a year.

There are some interesting properties of this calendar.

- Every month has the same number of days.
- There are 360 days a year---very close to the Gregorian calendar on Earth.
- Two months are dedicated to each season---one month for the beginning and one
  for the end.
- Every month has a "central" day to it---the fifth day of the third week.

You can find more about Gresha on page #ts-link(<continent-of-gresha>)[#label-page-num(locate, <continent-of-gresha>)].
