#import "/lib.typ": *

#huge-title[= Marketplace]

#large-title[== Transportation Services]

Costs shown are per person.

#table(
  columns: 2,
  [*Method*],
  [*Cost*],
  [Wagon],
  [50g],
  [Wagon],
  [50g],
  [Wagon],
  [50g],
  [Wagon],
  [50g],
  [Wagon],
  [50g],
)
