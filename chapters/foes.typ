#import "/lib.typ": *

#huge-title[= Foes <foes>]

Cairn already has a #ts-link("https://cairnrpg.com/resources/monsters/")[monster list]
that covers many traditional fantasy foes that can easily be used in a dark
fantasy setting, but the ones included are meant to further bolster tone of the
setting.

#large-title[== Humanoids]

#cairn-stat-block(load-stat("bandit"))[
  #item-title[=== Bandit <foe.bandit>]
]

#cairn-stat-block(load-stat("conscript"))[
  #item-title[=== Conscript <foe.conscript>]
]

#cairn-stat-block(load-stat("highwayman"))[
  #item-title[=== Highwayman <foe.highwayman>]
]

#cairn-stat-block(load-stat("imperial-infantry"))[
  #item-title[=== Imperial Infantry <foe.imperial-infantry>]
]

#cairn-stat-block(load-stat("imperial-knight"))[
  #item-title[=== Imperial Knight <foe.imperial-knight>]
]

#cairn-stat-block(load-stat("imperial-pacifier"))[
  #item-title[=== Imperial Pacifier <foe.imperial-pacifier>]
]

#cairn-stat-block(load-stat("mercenary-soldier"))[
  #item-title[=== Mercenary Soldier <foe.mercenary-soldier>]
]

#cairn-stat-block(load-stat("mountain-dwarf-defender"))[
  #item-title[=== Mountain Dwarf Defender <foe.mountain-dwarf-defender>]
]

#cairn-stat-block(load-stat("northern-cavalry"))[
  #item-title[=== Northern Cavalry <foe.northern-cavalry>]
]

#cairn-stat-block(load-stat("northern-elite-soldier"))[
  #item-title[=== Northern Elite Soldier <foe.northern-elite-soldier>]
]

#cairn-stat-block(load-stat("northern-heavy-infantry"))[
  #item-title[=== Northern Heavy Infantry <foe.northern-heavy-infantry>]
]

#cairn-stat-block(load-stat("northern-soldier"))[
  #item-title[=== Northern Solider <foe.northern-soldier>]
]

#cairn-stat-block(load-stat("pirate"))[
  #item-title[=== Pirate <foe.pirate>]
]

#cairn-stat-block(load-stat("rebel-archer"))[
  #item-title[=== Rebel Archer <foe.rebel-archer>]
]

#cairn-stat-block(load-stat("rebel-marauder"))[
  #item-title[=== Rebel Marauder <foe.rebel-marauder>]
]

#cairn-stat-block(load-stat("rebel-veteran"))[
  #item-title[=== Rebel Veteran <foe.rebel-veteran>]
]

#cairn-stat-block(load-stat("secret-agent"))[
  #item-title[=== Secret Agent <foe.secret-agent>]
]

#large-title[== Beasts]

#cairn-stat-block(load-stat("warg"))[
  #item-title[=== Warg <foe.warg>]
]

#cairn-stat-block(load-stat("werewolf"))[
  #item-title[=== Werewolf <foe.werewolf>]
]

#cairn-stat-block(load-stat("wolf"))[
  #item-title[=== Wolf <foe.wolf>]
]

#large-title[== Cursed Ones]

#cairn-stat-block(load-stat("amarok"))[
  #item-title[=== Amarok <foe.amarok>]
]

#cairn-stat-block(load-stat("botchling"))[
  #item-title[=== Botchling <foe.botchling>]
]

#cairn-stat-block(load-stat("preta"))[
  #item-title[=== Preta <foe.preta>]
]

#cairn-stat-block(load-stat("striga"))[
  #item-title[=== Striga <foe.striga>]
]

#large-title[== Draconids]

#cairn-stat-block(load-stat("basilisk"))[
  #item-title[=== Basilisk <foe.basilisk>]
]

#cairn-stat-block(load-stat("bukuvak"))[
  #item-title[=== Bukuvak <foe.bukuvak>]
]

#cairn-stat-block(load-stat("cockatrice"))[
  #item-title[=== Cockatrice <foe.cockatrice>]
]

#cairn-stat-block(load-stat("dragon"))[
  #item-title[=== Dragon <foe.dragon>]
]

#cairn-stat-block(load-stat("phoenix"))[
  #item-title[=== Phoenix <foe.phoenix>]
]

#cairn-stat-block(load-stat("wyvern"))[
  #item-title[=== Wyvern <foe.wyvern>]
]

#large-title[== Elementals]

#cairn-stat-block(load-stat("elemental-earth"))[
  #item-title[=== Elemental, Earth <foe.elemental-earth>]
]

#cairn-stat-block(load-stat("elemental-fire"))[
  #item-title[=== Elemental, Fire <foe.elemental-fire>]
]

#cairn-stat-block(load-stat("elemental-ice"))[
  #item-title[=== Elemental, Ice <foe.elemental-ice>]
]

#cairn-stat-block(load-stat("gargoyle"))[
  #item-title[=== Gargoyle <foe.gargoyle>]
]

#cairn-stat-block(load-stat("golem"))[
  #item-title[=== Golem <foe.golem>]
]

#large-title[== Hybrids]

#cairn-stat-block(load-stat("berberoka"))[
  #item-title[=== Berberoka <foe.berberoka>]
]

#cairn-stat-block(load-stat("griffin"))[
  #item-title[=== Griffin <foe.griffin>]
]

#cairn-stat-block(load-stat("harpy"))[
  #item-title[=== Harpy <foe.harpy>]
]

#cairn-stat-block(load-stat("manticore"))[
  #item-title[=== Manticore <foe.manticore>]
]

#cairn-stat-block(load-stat("siren"))[
  #item-title[=== Siren <foe.siren>]
]

#cairn-stat-block(load-stat("succubi"))[
  #item-title[=== Succubi <foe.succubi>]
]

#cairn-stat-block(load-stat("uktena"))[
  #item-title[=== Uktena <foe.uktena>]
]

#large-title[== Insectoids]

#cairn-stat-block(load-stat("arachas"))[
  #item-title[=== Arachas <foe.arachas>]
]

#cairn-stat-block(load-stat("endrega"))[
  #item-title[=== Endrega <foe.endrega>]
]

#cairn-stat-block(load-stat("guvorag"))[
  #item-title[=== Guvorag <foe.guvorag>]
]

#cairn-stat-block(load-stat("lopustre"))[
  #item-title[=== Lopustre <foe.lopustre>]
]

#cairn-stat-block(load-stat("zeugl"))[
  #item-title[=== Zeugl <foe.zeugl>]
]

#large-title[== Necrophages]

#cairn-stat-block(load-stat("devourer"))[
  #item-title[=== Devourer <foe.devourer>]
]

#cairn-stat-block(load-stat("drowner"))[
  #item-title[=== Drowner <foe.drowner>]
]

#cairn-stat-block(load-stat("foglet"))[
  #item-title[=== Foglet <foe.foglet>]
]

#cairn-stat-block(load-stat("ghoul"))[
  #item-title[=== Ghoul <foe.ghoul>]
]

#item-title[==== Hags <foe.hags>]

#cairn-stat-block(load-stat("hag-grave"))[
  ===== Grave Hag <foe.hag-grave>
]

#cairn-stat-block(load-stat("hag-water"))[
  ===== Water Hag <foe.hag-water>
]

#cairn-stat-block(load-stat("rotfiend"))[
  #item-title[=== Rotfiend <foe.rotfiend>]
]

#large-title[== Ogroids]

#cairn-stat-block(load-stat("anopheli"))[
  #item-title[=== Anopheli <foe.anopheli>]
]

#cairn-stat-block(load-stat("cyclops"))[
  #item-title[=== Cyclops <foe.cyclops>]
]

#cairn-stat-block(load-stat("nekker"))[
  #item-title[=== Nekker <foe.nekker>]
]

#cairn-stat-block(load-stat("ogre"))[
  #item-title[=== Ogre <foe.ogre>]
]

#cairn-stat-block(load-stat("troll-rock"))[
  #item-title[=== Troll, Rock <foe.troll-rock>]
]

#cairn-stat-block(load-stat("vodnik"))[
  #item-title[=== Vodnik <foe.vodnik>]
]

#large-title[== Relicts]

#cairn-stat-block(load-stat("chort"))[
  #item-title[=== Chort <foe.chort>]
]

#cairn-stat-block(load-stat("doppler"))[
  #item-title[=== Doppler <foe.doppler>]
]

#cairn-stat-block(load-stat("dryad"))[
  #item-title[=== Dryad <foe.dryad>]
]

#cairn-stat-block(load-stat("fiend"))[
  #item-title[=== Fiend <foe.fiend>]
]

#cairn-stat-block(load-stat("leshen"))[
  #item-title[=== Leshen <foe.leshen>]
]

#cairn-stat-block(load-stat("nereid"))[
  #item-title[=== Nereid <foe.nereid>]
]

#cairn-stat-block(load-stat("rusalki"))[
  #item-title[=== Rusalki <foe.rusalki>]
]

#cairn-stat-block(load-stat("shaelmaar"))[
  #item-title[=== Shaelmaar <foe.shaelmaar>]
]

#cairn-stat-block(load-stat("sylvan"))[
  #item-title[=== Sylvan <foe.sylvan>]
]

#large-title[== Specters]

#cairn-stat-block(load-stat("barghest"))[
  #item-title[=== Barghest <foe.barghest>]
]

#cairn-stat-block(load-stat("draug"))[
  #item-title[=== Draug <foe.draug>]
]

#cairn-stat-block(load-stat("draugir"))[
  #item-title[=== Draugir <foe.draugir>]
]

#cairn-stat-block(load-stat("noonwraith"))[
  #item-title[=== Noonwraith <foe.noonwraith>]
]

#cairn-stat-block(load-stat("pesta"))[
  #item-title[=== Pesta <foe.pesta>]
]

#cairn-stat-block(load-stat("wraith"))[
  #item-title[=== Wraith <foe.wraith>]
]

#large-title[== Vampires]

#cairn-stat-block(load-stat("alp"))[
  #item-title[=== Alp <foe.alp>]
]

#cairn-stat-block(load-stat("fleder"))[
  #item-title[=== Fleder <foe.fleder>]
]

#cairn-stat-block(load-stat("higher-vampire"))[
  #item-title[=== Higher Vampire <foe.higher-vampire>]
]

#cairn-stat-block(load-stat("katakan"))[
  #item-title[=== Katakan <foe.katakan>]
]

#large-title[== The Hunt]

#cairn-stat-block(load-stat("hunt-hound"))[
  #item-title[=== Hound of the Hunt <foe.hunt-hound>]
]

#cairn-stat-block(load-stat("hunt-rider"))[
  #item-title[=== Rider of the Hunt <foe.hunt-rider>]
]

#cairn-stat-block(load-stat("hunt-steed"))[
  #item-title[=== Steed of the Hunt <foe.hunt-steed>]
]
