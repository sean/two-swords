#import "/lib.typ": *

#huge-title[= Rules]

#large-title[== Recovery <recovery>]

#item-title[=== Recovering Lost HP <recovering-lost-hp>]

To recover full HP outside of combat, take a drink of water. If you don't have
access to a supply of water such as a well or creek, you must use personal water
containers such as a waterskin. Personal water containers have a limited amount
of uses to drink from.

If you do not have any water to drink from, you can only recover half HP
(rounded down).

#item-title[=== Recovering Attribute Damage <recovering-attribute-damage>]

#note[
  This is not the only way to recover attribute damage---the Warden can think of
  any number of ways to recover damage.
]

When sleeping in a safe location, you can choose an attribute to recover 1 point
of damage from. To recover damage to any other attribute in the same sleep you
must roll make a save with that attribute. If you succeed, it recovers 1 point
of damage.
